import jwt from 'jsonwebtoken';

export default ({ config }) => ({
  encode: (payload, callback) => callback(jwt.sign(payload, config.jwtSecret)),
  decode: (token, callback) => callback(jwt.verify(token, config.jwtSecret)),
});
