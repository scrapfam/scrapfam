import axios from 'axios';
import config from '../config/config.json';

const FCM_Url = 'https://fcm.googleapis.com/fcm/send';

const sendNotification = (formData) => {
  return axios.post(FCM_Url, formData, {
    headers: { Authorization: config.firebaseKey }
  });
};

export default sendNotification;
