import mongoose from 'mongoose';

export default ({ config }, callback) => {
  mongoose.Promise = global.Promise;
  mongoose.connect(`${process.env.DATABASE_URL}${process.env.DATABASE_NAME}`, {
    useMongoClient: true,
  });
  const db = mongoose.connection;
  db.on('error', (e) => {
    console.log('Mongoose Error:', e); // eslint-disable-line no-console
  });
  db.once('open', () => {
    console.log('MongoDB connected'); // eslint-disable-line no-console
    callback(db);
  });
};
