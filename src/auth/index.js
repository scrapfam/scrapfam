import { Router } from 'express';

import login from './LoginRoute';
import signup from './SignupRoute';

export default ({ config, jwt, db }) => {
  const router = Router();

  router.use('/login', login({ config, jwt, db }));
  router.use('/signup', signup({ config, db }));

  return router;
};
