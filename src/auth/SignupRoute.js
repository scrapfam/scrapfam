import { Router } from 'express';
import SignupController from './SignupController';

export default () => {
  const auth = Router();
  auth.post('/', SignupController.createAccount);

  return auth;
};
