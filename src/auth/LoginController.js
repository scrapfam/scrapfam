import { Router } from 'express';
import PersonModel from '../api/modules/person/PersonModel';
import UserModel from '../api/modules/user/UserModel';

import handleError from '../api/modules/utils/handleError';

let controller = {};

controller.localLogin = async function({ body }, res, jwt) {
  try {
    const person = await PersonModel.findOne({ email: body.email }).lean();
    if (!person) {
      res.sendStatus(401);
      return;
    }

    const user = await UserModel.findOne({ person: person._id });
    user.comparePassword(body.password, (passwordErr, isValid) => {
      if (passwordErr) {
        res.sendStatus(400);
        return;
      }
      if (!isValid) {
        res.sendStatus(401);
        return;
      }
      jwt.encode(
        {
          personId: person._id,
          defaultTagId: person.defaultTagId,
        },
        token => {
          const {
            _id, // useless
            ...rest
          } = person;
          res.json({
            token,
            personId: user.person,
            ...rest,
          });
        },
      );
    });
  } catch (err) {
    console.log(err);
    handleError(res)(err);
  }
};

export default controller;
