import { Router } from 'express';
import LoginController from './LoginController';

export default ({ jwt }) => {
  const auth = Router();
  auth.post('/', function (req, res) {
    LoginController.localLogin(req, res, jwt);
  });

  return auth;
};
