import httpStatus from 'http-status';
import regeneratorRuntime from 'regenerator-runtime';

import handleError from '../api/modules/utils/handleError';

import UserModel from '../api/modules/user/UserModel';
import PersonModel from '../api/modules/person/PersonModel';
import TagModel from '../api/modules/tag/TagModel';
import { OWNER } from '../api/modules/tag/tagUtils';
import NotificationModel from '../api/modules/notification/NotificationModel';
import PersonFriendsModel from '../api/modules/person_friends/PersonFriendsModel';

let controller = {};

controller.createAccount = async (req, res) => {
  const {
    email,
    password,
    ...others,
  } = req.body;

    try {
      let person = await PersonModel.findOne({ email });
      if(person) {
        res.json(httpStatus.ALREADY_REPORTED);
        return;
      }
      person = new PersonModel({ ...others, email });
      const user = new UserModel({ password: password, person: person._id });
      const userNotifications = new NotificationModel({ person: person._id });
      const personFriends = new PersonFriendsModel({ person: person._id });

      const tag = new TagModel({ name: 'Aleatórias', sharedWith: [{ person: person._id, role: OWNER }] });

      person.defaultTagId = tag._id;

      await person.save();
      await user.save();
      await tag.save();
      await userNotifications.save();
      await personFriends.save();

      res.json(httpStatus.CREATED);
    } catch (err) {
      console.log(err);
      handleError(res)(err);
    }
};

export default controller;
