import httpStatus from 'http-status';
import mongoose from 'mongoose';

import ItemModel from './ItemModel';
import TagModel from '../tag/TagModel';
import PersonTagsModel from '../person_tags/PersonTagsModel';
import handleError from '../utils/handleError';

let controller = {};

controller.create = async function({ body, token }, res) {
  const {
    listTags,
    ...others
  } = body;

  try {
    const itemModel = new ItemModel({ ...others });
    if (!listTags || listTags.length < 1) {
      const tag = await TagModel.findById(token.defaultTagId);
      tag.listItems.push({ item: itemModel._id });
      await tag.save();
    } else {
      for (let id of listTags) {
        const tag = await TagModel.findById(id);
        tag.listItems.push({ item: itemModel._id });
        await tag.save();
      }
    }

    await itemModel.save();
    res.status(httpStatus.CREATED).json(itemModel._id);
  } catch (err) {
    console.log(err);
    handleError(res)(err);
  }
}

controller.update = async function({ body, params }, res) {
  const {
    listTags
  } = body;

  const {
    id
  } = params;

  try {
    for (let tagId of listTags) {
      const tag = await TagModel.findById(tagId);
      tag.listItems.push({ item: id });
      await tag.save();
    }
    res.sendStatus(httpStatus.ACCEPTED);
  } catch (err) {
    console.log(err);
    handleError(res)(err);
  }
}

controller.delete = async function({ params }, res) {
  const {
    itemId,
    tagId,
  } = params;

  try {
    await ItemModel.remove({ _id: itemId });
    const tag = await TagModel.findById(tagId);
    tag.listItems.pull({ item: itemId });
    await tag.save();
  
    res.json(httpStatus.ACCEPTED);

  } catch (err) {
    console.log(err);
    handleError(res)(err);
  }
}

export default controller;
