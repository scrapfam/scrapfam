import mongoose, { Schema } from 'mongoose';
import { TYPE, PHOTO } from './itemUtils';

const itemSchema = new Schema({
  clientId: {
    type: String,
    trim: true,
  },
  url: {
    type: String,
    required: true
  },
  type: {
    type: String,
    enum: TYPE,
    default: PHOTO,
    required: true
  }
}, { versionKey: false });

const Item = mongoose.model('Item', itemSchema);

export default Item;
