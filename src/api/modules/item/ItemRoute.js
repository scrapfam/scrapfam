import { Router } from 'express';
import ItemController from './ItemController';

export default () => {
  let api = Router();

  api.post('/', ItemController.create);
  api.delete('/:itemId/:tagId', ItemController.delete);
  api.put('/:id', ItemController.update);
  // api.get('/:id', ItemController.find);

  return api;
};
