export const AUDIO = 'AUDIO';
export const VIDEO = 'VIDEO';
export const PHOTO = 'PHOTO';

export const TYPE = [AUDIO, VIDEO, PHOTO];