import HTTPStatus from 'http-status';

export default function (res) {
  return (err = {}) => {
    if (err.httpCode) {
      res.sendStatus(err.httpCode);
      return;
    }
    if (err.name === 'ValidationError') {
      res.sendStatus(HTTPStatus.UNPROCESSABLE_ENTITY);
      return;
    }
    res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
  };
}
