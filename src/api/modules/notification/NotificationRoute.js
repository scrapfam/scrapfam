import { Router } from 'express';
import NotificationController from './NotificationController';

export default () => {
  let api = Router();

  api.get('/', NotificationController.findAll);
  api.put('/:id', NotificationController.update);
  return api;
};
