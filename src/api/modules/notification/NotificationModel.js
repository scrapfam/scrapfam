import mongoose, { Schema } from 'mongoose';
import { LIST_ACTIONS } from './notificationUtils';

const notificationModel = new Schema({
  person: {
    type: Schema.Types.ObjectId,
    ref: 'Person',
    required: true,
  },
  listNotifications: [{
    person: {
      type: Schema.Types.ObjectId,
      ref: 'Person',
    },
    action: {
      type: String,
      enum: LIST_ACTIONS
    },
    tag: {
      type: Schema.Types.ObjectId,
      ref: 'Tag',
    },
    date: {
      type: Number,
    },
    isViewed: {
      type: Boolean,
      default: false
    }
  }],
}, { versionKey: false });

const Person = mongoose.model('Notifications', notificationModel);

export default Person;
