export const ACCEPTING_FRIENDSHIP = "ACCEPTING_FRIENDSHIP";
export const RECEIVING_FRIENDSHIP = "RECEIVING_FRIENDSHIP";
export const SHARED_TAG = "SHARED_TAG";

export const ACTIONS = {
  ACCEPTING_FRIENDSHIP,
  RECEIVING_FRIENDSHIP,
  SHARED_TAG
}

export const normalizeNotificationsToClient = (notifications) => {
  return notifications.map((notification) => {
    const {
      _id,
      person,
      tag,
      ...others
    } = notification;
    const tagReturned = tag ? {
      serverId: tag._id,
      clientId: tag.clientId,
      name: tag.name
    } : null; // PRECISO TROCAR PRA TAG
    
    const notificationReturned = {
      person: {
        id: person._id,
        photo: person.photo,
        firstName: person.firstName,
        lastName: person.lastName,
      },
      id: _id,
      ...others,
    };

    notificationReturned.tag = tagReturned;
    return notificationReturned;
  });
}

export const normalizeNotificationToClient = (notification) => {
  const {
    action,
    date,
    _id : id,
    isViewed,
    person,
    tag,
  } = notification;

  return {
    action,
    date,
    id,
    isViewed,
  }
};

export const LIST_ACTIONS = [ ACCEPTING_FRIENDSHIP, RECEIVING_FRIENDSHIP, SHARED_TAG ];
