import httpStatus from 'http-status';

import NotificationModel from './NotificationModel';
import handleError from '../utils/handleError';

import { normalizeNotificationsToClient, normalizeNotificationToClient } from './notificationUtils';

let controller = {};

controller.findAll = function({ token }, res) {
  const query = { person: token.personId };
  NotificationModel.findOne(query, '-_id')
    .populate('listNotifications.person', '_id firstName lastName photo')
    .populate('listNotifications.tag', '_id name clientId')
    .lean()
    .then((data) => {
      if (data) {
        res.status(httpStatus.ACCEPTED).json(normalizeNotificationsToClient(data.listNotifications));
      } else {
        res.status(httpStatus.ACCEPTED).json([]);
      }
    }, handleError(res));
};

controller.update = async function(req, res) {
  const { body, params, token } = req;

  const query = { person: token.personId };

  const notificationNode = await NotificationModel.findOne(query);
  const notificationChanged = notificationNode.listNotifications.id(params.id);
  notificationChanged.isViewed = body.isViewed;

  await notificationNode.save();
  res.status(httpStatus.ACCEPTED).json(normalizeNotificationToClient(notificationChanged.toObject()));
};

export default controller;
