import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt';

const SALT_WORK_FACTOR = 10;

const userSchema = new Schema({
  password: {
    type: String,
    required: true,
  },
  person: {
    type: Schema.Types.ObjectId,
    ref: 'Person',
    required: true,
  }
}, { versionKey: false });

// Hashing http://stackoverflow.com/questions/14588032/mongoose-password-hashing
userSchema.pre('save', function preSaveUser(next) {
  // only hash the password if it has been modified (or is new)
  if (!this.isModified('password')) {
    next();
    return;
  }
  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, (saltErr, salt) => {
    if (saltErr) {
      next(saltErr);
      return;
    }
    // hash the password using our new salt
    bcrypt.hash(this.password, salt, (hashErr, hash) => {
      if (hashErr) {
        next(hashErr);
        return;
      }

      // override the cleartext password with the hashed one
      this.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = function comparePassword(candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
      if (err) {
        callback(err);
        return;
      }
      callback(null, isMatch);
    },
  );
};


const User = mongoose.model('User', userSchema);

export default User;
