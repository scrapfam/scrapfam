import { Router } from 'express';
import UserController from './UserController';

export default () => {
	let api = Router();

	api.post('/', UserController.create);
	api.put('/:id', UserController.update);

	return api;
}
