import UserModel from './UserModel';
import handleError from '../utils/handleError';

let controller = {};

controller.create = function({ body }, res) {
  const user = new UserModel(body);
  user.save(user).then(user => res.json(user), handleError(res));
}

controller.update = function({ body, params }, res) {
  Object.assign(body, params.id);
  const query = { _id: params.id };
  UserModel.update(query, body).then(user => res.json(user), handleError(res));
}

export default controller;
