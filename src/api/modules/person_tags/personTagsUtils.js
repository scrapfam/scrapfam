export const ADMIN = 'ADMIN';
export const OWNER = 'OWNER';
export const VIEWER = 'VIEWER';

export const normalizeSharedWithToApi = (sharedWith) => {
  return sharedWith.map((item) => {
    return {
      person: item.personId,
      role: item.role
    };
  });
};

export default [ADMIN, OWNER, VIEWER];