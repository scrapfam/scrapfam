import mongoose, { Schema } from 'mongoose';

const personTagsSchema = new Schema({
  person: {
    type: Schema.Types.ObjectId,
    ref: 'Person',
    required: true,
  },
  tags: [{
    _id: false,
    tag: {
      type: Schema.Types.ObjectId,
      ref: 'Tag',
    }
  }],
});

const PersonTags = mongoose.model('PersonTags', personTagsSchema);

export default PersonTags;
// shared_with - Only thumbnail???
