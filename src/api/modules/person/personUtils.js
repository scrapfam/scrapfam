export const normalizePersonByEmailToClient = (person) => {
  return {
    firstName: person.firstName,
    lastName: person.lastName,
    id: person._id,
    email: person.email,
    photo: person.photo,
  };
};

export const normalizeMyProfileToClient = (person) => {
  return {
    firstName: person.firstName,
    lastName: person.lastName,
    photo: person.photo,
    birthDate: person.birthDate,
    id: person._id,
    email: person.email,
  };
};