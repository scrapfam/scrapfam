import { Router } from 'express';
import PersonController from './PersonController';

export default () => {
  let api = Router();

  // api.post('/', PersonController.create);
  api.get('/my-profile', PersonController.myProfile);
  // api.get('/:id', PersonController.findOne);
  api.patch('/', PersonController.findPersonByEmail);
  api.put('/notification-id', PersonController.updateFirebaseNotificationId);
  api.put('/', PersonController.update);

  return api;
};
