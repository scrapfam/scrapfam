import mongoose, { Schema } from 'mongoose';

const personSchema = new Schema({
  firstName: {
    type: String,
    required: true,
    trim: true
  },
  lastName: {
    type: String,
    required: true,
    trim: true
  },
  email: {
    type: String,
    required: true,
    index: { unique: true },
    trim: true
  },
  birthDate: {
    type: String,
    default: null
  },
  photo: {
    type: String,
    default: '',
  },
  firebaseNotificationId: {
    type: String,
    trim: true
  },
  defaultTagId: {
    type: Schema.Types.ObjectId,
    ref: 'Tag',
  }
}, { versionKey: false });

const Person = mongoose.model('Person', personSchema);

export default Person;
