import httpStatus from 'http-status';

import PersonModel from './PersonModel';
import PersonFriendsModel from '../person_friends/PersonFriendsModel';

import { normalizePersonByEmailToClient, normalizeMyProfileToClient } from './personUtils';
import handleError from '../utils/handleError';

let controller = {};

controller.findOne = function({ params }, res) {
  const query = { _id: params.id };
  PersonModel.findOne(query)
    .then(personEntity => res.status(httpStatus.ACCEPTED).json(personEntity), handleError(res));
}

controller.myProfile = async function({ token }, res) {
  try {
    const personModel = await PersonModel.findById(token.personId).lean();
    const personFriendsModel = await PersonFriendsModel.findOne({ person: token.personId }).lean();
    res.status(httpStatus.ACCEPTED).json({
      person: normalizeMyProfileToClient(personModel),
      quantityOfFriends: personFriendsModel.friends.length,
    });
    handleError(res)
  } catch (err) {
    console.log(err);
    handleError(res)(err);
  }
}

controller.findPersonByEmail = async function({ body, token }, res) {
  const query = { email: body.email };
  try {
    const person = await PersonModel.findOne(query);
    const normalizedPerson = normalizePersonByEmailToClient(person);
    if(!normalizedPerson) {
      res.status(httpStatus.ACCEPTED).json(null);
      return;
    }

    const myFriends = await PersonFriendsModel.findOne({ person: token.personId });

    const hasSentRequest = myFriends.pendingTo.find((item) => {
      return `${item.person}` == `${normalizedPerson.id}`;
    });

    if (hasSentRequest) {
      res.status(httpStatus.ACCEPTED).json({ ...normalizedPerson, hasSent: true });
      return;
    }

    const isFriend = myFriends.friends.find((item) => {
      return `${item.person}` == `${normalizedPerson.id}`;
    });

    if (isFriend) {
      res.status(httpStatus.ACCEPTED).json({ ...normalizedPerson, isFriend: true });
      return;
    }

    res.status(httpStatus.ACCEPTED).json(normalizedPerson);
  } catch(err) {
    console.log(err);
    handleError(res)(err);
  }
}

controller.create = function({ body }, res) {
  const person = new PersonModel(body);
  person.save(person).then(() => res.status(httpStatus.ACCEPTED).json(person), handleError(res));
}

controller.updateFirebaseNotificationId = function({ body, token }, res) {
  const query = { _id: token.personId };
  const update = { firebaseNotificationId: body.firebaseNotificationId };
  PersonModel.update(query, update).then(person => res.json(httpStatus.ACCEPTED), handleError(res));
}

controller.update = function({ body, token }, res) {
  Object.assign(body, token.personId);
  const query = { _id: token.personId };
  PersonModel.findOneAndUpdate(query, body)
    .then(person => res.json(httpStatus.ACCEPTED), handleError(res));
}

export default controller;
