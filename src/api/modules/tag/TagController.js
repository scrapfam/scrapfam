import httpStatus from 'http-status';

import TagModel from './TagModel';
import NotificationModel from '../notification/NotificationModel';
import PersonModel from '../person/PersonModel';
import sendNotification from '../../../utils/notifications';
import { ACTIONS } from '../notification/notificationUtils';

import {
  OWNER,
  normalizeSharedWithToApi,
  normalizeListTagsToClient,
  normalizeTagToClient,
  normalizeFindOneTagToClient,
  arrayComparetor,
} from './tagUtils';

import handleError from '../utils/handleError';

let controller = {};

controller.findOne = function({ params }, res) { // atualizar o payload de retorno de uma tag
  TagModel.findById(params.id)
    .populate('listItems.item')
    .populate('sharedWith.person', '_id photo firstName lastName')
    .lean()
    .then(tagEntity => {
      if (!tagEntity) {
        res.json({});
      }
      res.json(normalizeTagToClient(tagEntity))
    }, handleError(res));
}

controller.findAll = function(req, res) { // NÃO RETORNAR A LISTA DE ITENS
  const query = { person: req.token.personId };
  TagModel.find({ 'sharedWith.person': req.token.personId }).lean()
  .populate({
    path: 'sharedWith.person',
    select: '_id photo firstName lastName',
  })
  .then((data) => {
    res.json(normalizeListTagsToClient(data));
  }, handleError(res));
}

controller.create = async function({ token, body }, res) {
  const {
    ...others,
  } = body;

  try {
    const tag = new TagModel({ ...others, sharedWith: [{ person: token.personId, role: OWNER }] });

    await tag.save();
    res.status(httpStatus.CREATED).json(normalizeTagToClient(tag.toObject()));

  } catch (err) {
    console.log(err);
    handleError(res)(err);
  }
}

controller.update = async function({ body, params, token }, res) {
  const {
    sharedWith, // [ { personId, role } ]
  } = body;

  const {
    id
  } = params;

  try {
    if (newPersons && newPersons.length > 0) {
      const tag = await TagModel.findOne(createQuery('_id', id));
      const normalizedSharedWith = normalizeSharedWithToApi(sharedWith);
      const newPersons = arrayComparetor(tag.sharedWith, normalizedSharedWith);
      tag.sharedWith = normalizedSharedWith;

      await tag.save();

      res.json({});
    } else {
      res.json('Você deve compartilhar com no mínimo um usuário');
    }

    let mySelf = await PersonModel.findOne(createQuery('_id', token.personId ));

    if (newPersons && newPersons.length) {
      for (let friend of newPersons) {
        if (friend.person != token.personId) {

          let person = await PersonModel.findOne(createQuery('_id', friend.person ));
          
// processo notificação logo abaixo //
          const formData = {
            to: person.firebaseNotificationId,
            data: {
              text: `<strong>${mySelf.firstName} ${mySelf.lastName}</strong>
                compartilhou a tag <strong>${tag.name}</strong> com você`,
              photo: mySelf.photo, // ex: nome de quem enviou a solicitação
              tag: tag._id,
              action: ACTIONS.SHARED_TAG
              }
            };
            sendNotification(formData)
              .then(async (response) => {
                let notifications = await NotificationModel.findOne(createQuery('person', friend.person ));
                notifications.listNotifications.push({
                person: token.personId,
                tag: tag._id,
                action: ACTIONS.SHARED_TAG,
                date: new Date().getTime()
              });
              await notifications.save();
            })
            .catch((err) => {
              console.log(err);
            });
// processo notificação logo acima //
        }
      }
    }
  } catch (err) {
    console.log(err);
    handleError(res)(err);
  }
}

function createQuery(key, value) {
  return { [key]: value };
}

export default controller;
