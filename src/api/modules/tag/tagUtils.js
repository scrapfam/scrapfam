export const ADMIN = 'ADMIN';
export const OWNER = 'OWNER';
export const VIEWER = 'VIEWER';

export const normalizeSharedWithToApi = (sharedWith) => {
  return sharedWith.map((item) => {
    return {
      person: item.personId,
      role: item.role
    }
  })
}

export const normalizeListTagsToClient = (tags) => {
  return tags.map((tag) => {
    return {
      name: tag.name,
      coverUrl: tag.coverUrl,
      serverId: tag._id,
      clientId: tag.clientId,
      sharedWith: tag.sharedWith.map(normalizeSharedWithToClient),
    }
  })
}

export const normalizeTagToClient = (tag) => {
  const {
    _id,
    sharedWith,
    listItems,
    ...others
  } = tag;
  return {
    serverId: _id,
    listItems: listItems.map(normalizeListItems),
    sharedWith: sharedWith.map(normalizeSharedWithToClient),
    ...others,
  }
}

export const normalizeFindOneTagToClient = (tag) => {
  const {
    _id,
    sharedWith,
    listItems,
    ...others
  } = tag;
  return {
    serverId: _id,
    listItems: listItems.map(normalizeListItems),
    sharedWith: sharedWith.map(normalizeSharedWithToClient),
    ...others,
  }
}

const normalizeSharedWithToClient = (item) => {
  const {
    person: {
      _id: personId,
      photo,
      firstName,
      lastName,
    },
    role,
  } = item;
  return {
    personId,
    photo,
    firstName,
    lastName,
    role
  };
}

const normalizeListItems = (item) => {
  const {
    _id,
    type,
    url,
    clientId
  } = item.item || {};

  return {
    serverId: _id,
    clientId,
    type,
    url,
  }
}

export const ROLES = [ADMIN, OWNER, VIEWER];

export function arrayComparetor (fromDb, fromClient) {
  function comparer(fromClient) {
    return (fromDb) => {
      return fromClient.filter((other) => {
        return other.person == fromDb.person
      }).length == 0;
    }
  }

  var onlyInDatabase = fromDb.filter(comparer(fromClient));
  var onlyInClient = fromClient.filter(comparer(fromDb));

  var result = onlyInDatabase.concat(onlyInClient);
  return result;

}
