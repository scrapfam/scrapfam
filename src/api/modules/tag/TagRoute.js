import { Router } from 'express';
import TagController from './TagController';

export default () => {
	let api = Router();

	api.get('/', TagController.findAll);
	api.get('/:id', TagController.findOne);
	api.post('/', TagController.create);
	api.put('/:id', TagController.update);
	// api.delete('/:id', TagController.delete);

	return api;
}
