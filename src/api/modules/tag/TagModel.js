import mongoose, { Schema } from 'mongoose';
import { ROLES } from './tagUtils';

const tagSchema = new Schema({
  clientId: {
    type: String,
    trim: true,
  },
  name: {
    type: String,
    trim: true,
    required: true,
  },
  coverUrl: {
    type: String,
    trim: true,
  },
  creationDate: {
    type: Number,
    default: new Date().getTime(),
  },
  listItems: [{
    _id: false,
    item: {
      type: Schema.Types.ObjectId,
      ref: 'Item',
      required: true,
    }
  }],
  sharedWith: [{
    _id: false,
    person: {
      type: Schema.Types.ObjectId,
      ref: 'Person',
    },
    role: {
      type: String,
      enum: ROLES,
    },
  }],
}, {versionKey: false});

const Tag = mongoose.model('Tag', tagSchema);

export default Tag;
