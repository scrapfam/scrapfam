import httpStatus from 'http-status';
import sendNotification from '../../../utils/notifications';

import PersonFriendsModel from './PersonFriendsModel';
import NotificationModel from '../notification/NotificationModel';

import {
  ACTIONS,
  normalizePendingFriends,
  normalizeResponseFriends,
} from './personFriendsUtils';
import { ACTIONS as NOTIFICATION_ACTIONS } from '../notification/notificationUtils';

import handleError from '../utils/handleError';

let controller = {};

controller.findAll = function(req, res) {
  const query = { person: req.token.personId };
  PersonFriendsModel.findOne(query, '-_id friends')
    .populate('friends.person', 'firstName lastName photo email')
    .then(
      persons =>
        res
          .status(httpStatus.ACCEPTED)
          .json(normalizeResponseFriends(persons.friends)),
      handleError(res),
    );
};

controller.findPendingFriendships = async function({ token }, res) {
  let pendingFrom = await PersonFriendsModel.findOne(
    { person: token.personId },
    'pendingFrom -_id',
  ).populate('pendingFrom.person');
  res
    .status(httpStatus.ACCEPTED)
    .json(normalizePendingFriends(pendingFrom ? pendingFrom.pendingFrom : []));
};

controller.addFriend = async function({ body, token }, res) {
  try {
    let myself = await PersonFriendsModel.findOne({
      person: token.personId,
    }).populate('person', 'firstName lastName photo');
    if (!myself) {
      myself = new PersonFriendsModel({ person: token.personId }).populate(
        'person',
        'firstName lastName photo',
      );
    }

    // verificando se já existe solicitação
    const hasAlreadyRequest = myself.pendingTo.find(item => {
      return item.person == body.personId;
    });
    // fim da verificação

    if (hasAlreadyRequest) {
      res.status(httpStatus.OK).json('Solicitação de amizade já enviada');
      return;
    }

    myself.pendingTo.push({ person: body.personId }); // adicionando pessoa que eu solicitei amizade
    await myself.save();

    let personAdded = await PersonFriendsModel.findOne({
      person: body.personId,
    }).populate('person', 'firebaseNotificationId');
    if (!personAdded) {
      personAdded = new PersonFriendsModel({ person: body.personId }).populate(
        'person',
        'firebaseNotificationId',
      );
    }

    personAdded.pendingFrom.push({ person: token.personId });
    await personAdded.save();

    res.json(httpStatus.CREATED);

    // processo notificação logo abaixo //
    const formData = {
      to: personAdded.person.firebaseNotificationId,
      data: {
        text: `<strong>${myself.person.firstName} ${myself.person
          .lastName}</strong>
          solicitou sua amizade`,
        photo: myself.person.photo, // ex: nome de quem enviou a solicitação
        tag: null,
        action: NOTIFICATION_ACTIONS.RECEIVING_FRIENDSHIP,
      },
    };
    sendNotification(formData)
      .then(async () => {
        let notifications = await NotificationModel.findOne({
          person: body.personId,
        });
        notifications.listNotifications.push({
          person: token.personId,
          tag: null,
          action: NOTIFICATION_ACTIONS.RECEIVING_FRIENDSHIP,
          date: new Date().getTime(),
        });
        await notifications.save();
      })
      .catch(err => {
        console.log(err);
      });
    // processo notificação logo acima //
  } catch (err) {
    console.log(err);
    handleError(res)(err);
  }
};

controller.update = async function({ token, params }, res) {
  const { id: person, action } = params;

  let myself = await PersonFriendsModel.findOne({
    person: token.personId,
  }).populate('person', 'firstName lastName photo'); // selecionando os atributos que serão passados no notification
  let personSelected = await PersonFriendsModel.findOne({ person }).populate(
    'person',
    'firebaseNotificationId firstName lastName photo',
  );

  switch (action) {
  case ACTIONS.ACCEPT: {
    const myselfHasFriend = myself.friends.find(friend => {
      return friend.person == person;
    });

    if (myselfHasFriend) {
      res.json('Já são amigos!');
      return;
    }
    const personSelectedHasFriend = personSelected.friends.find(friend => {
      return friend.person == token.personId;
    });

    if (personSelectedHasFriend) {
      res.json('Já são amigos!');
      return;
    }

    myself.friends.push({ person });
    myself.pendingFrom.pull({ person });
    await myself.save();

    personSelected.friends.push({ person: token.personId });
    personSelected.pendingTo.pull({ person: token.personId });
    await personSelected.save();

    res.json(httpStatus.ACCEPTED);

    const formData = {
      to: personSelected.person.firebaseNotificationId, // id de quem receberá a notificação
      data: {
        text: `<strong>${myself.person.firstName} ${myself.person
          .lastName}</strong>
        aceitou sua solicitação de amizade`,
        photo: myself.person.photo, // ex: nome de quem enviou a solicitação
        tag: null,
        action: NOTIFICATION_ACTIONS.ACCEPTING_FRIENDSHIP,
      },
    };

    sendNotification(formData)
      .then(async () => {
        let notifications = await NotificationModel.findOne({ person });
        notifications.listNotifications.push({
          person: token.personId,
          tag: null,
          action: NOTIFICATION_ACTIONS.ACCEPTING_FRIENDSHIP,
        });
        await notifications.save();
      })
      .catch(err => {
        console.log(err);
      });
    break;
  }

  case ACTIONS.REJECT:
    personSelected.pendingTo.pull({ person: token.personId });
    await personSelected.save();

    myself.pendingFrom.pull({ person });
    await myself.save();

    res.json(httpStatus.ACCEPTED);

    break;
  }
};

controller.delete = async function({ params, token }, res) {
  let myself = await PersonFriendsModel.findOne({ person: token.personId });
  let personSelected = await PersonFriendsModel.findOne({ person: params.id });

  myself.friends.pull({ person: params.id });
  await myself.save();

  personSelected.friends.pull({ person: token.personId });
  await personSelected.save();

  res.json(httpStatus.ACCEPTED);
};

export default controller;
