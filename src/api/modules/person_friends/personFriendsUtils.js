export const ACCEPT = 'accept';
export const REJECT = 'reject';

export const ACTIONS = { ACCEPT, REJECT };

export const normalizePendingFriends = (pendingFriends) => {
  return pendingFriends.map(item => {
    return {
      id: item.person._id,
      firstName: item.person.firstName,
      lastName: item.person.lastName,
      photo: item.person.photo,
      email: item.person.email,
    };
  });
};

export const normalizeResponseFriends = (friends) => {
  return friends.map(friend => {
    return {
      id: friend.person._id,
      firstName: friend.person.firstName,
      lastName: friend.person.lastName,
      photo: friend.person.photo,
    };
  });
};
