import { Router } from 'express';
import PersonFriendsController from './PersonFriendsController';

export default () => {
  let api = Router();

  api.get('/', PersonFriendsController.findAll);
  api.get('/pending', PersonFriendsController.findPendingFriendships);
  api.post('/', PersonFriendsController.addFriend);
  api.put('/:id/:action', PersonFriendsController.update);
  api.delete('/:id', PersonFriendsController.delete);

  return api;
};
