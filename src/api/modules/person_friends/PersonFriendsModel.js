import mongoose, { Schema } from 'mongoose';

const personFriends = new Schema({
  person: {
    type: Schema.Types.ObjectId,
    ref: 'Person',
    required: true,
  },
  friends: [{
    _id: false,
    person: {
      type: Schema.Types.ObjectId,
      ref: 'Person',
    },
  }],
  pendingTo: [{
    _id: false,
    person: {
      type: Schema.Types.ObjectId,
      ref: 'Person',
    },
  }],
  pendingFrom: [{
    _id: false,
    person: {
      type: Schema.Types.ObjectId,
      ref: 'Person',
    },
  }],
});

const Person = mongoose.model('PersonFriends', personFriends);

export default Person;
