import { Router } from 'express';
import tags from './modules/tag/TagRoute';
import items from './modules/item/ItemRoute';
import persons from './modules/person/PersonRoute';
import friends from './modules/person_friends/PersonFriendsRoute';
import notifications from './modules/notification/NotificationRoute';

export default ({ config, db }) => {
	let api = Router();

	api.use('/friends', friends({ config, db }));
	api.use('/items', items({ config, db }));
	api.use('/persons', persons({ config, db }));
	api.use('/tags', tags({ config, db }));
	api.use('/notifications', notifications({ config, db }));

	return api;
}
