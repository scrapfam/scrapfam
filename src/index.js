import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import initializeDb from './config/DatabaseConnection';
import middleware from './middleware';
import routes from './api/Routes';
import config from './config/config.json';
import jwtUtils from './utils/jwtUtils';
import auth from './auth';

const app = express();
app.server = http.createServer(app);

const jwt = jwtUtils({ config });

// logger
app.use(morgan('dev'));

// 3rd party middleware
app.use(cors());

app.use(bodyParser.json({ limit : config.bodyLimit }));

// connect to db
initializeDb({config}, db => {

  app.get('/', (req, res) => {
    res.json('Bem vindo ao ScrapFam API!');
  });

  // auth middleware
  app.use('/auth', auth({ config, jwt, db }));

  // internal middleware
  app.use(middleware({ config, db, jwt }));

  // api router
  app.use('/api', routes({ config, db }));

  app.server.listen(process.env.SERVER_PORT || config.port);
  console.log(`Started on port ${app.server.address().port}`);
});

export default app;

// "mongoServer": "mongodb://admin:admin@ds155651.mlab.com:55651/",
// "database": "scrapfam-mlab"