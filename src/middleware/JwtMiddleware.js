import httpStatus from 'http-status';

export default ({ jwt }) => (
  (req, res, next) => {
    const auth = req.headers.authorization;
    if (!auth) {
      res.status(httpStatus.UNAUTHORIZED).json(httpStatus[401]);
      return;
    }
    jwt.decode(auth, (token) => {
      req.token = token;
      next();
    });
  }
);
