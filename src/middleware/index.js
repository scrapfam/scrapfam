import { Router } from 'express';

import jwtInjectMiddleware from './JwtMiddleware';

export default ({ jwt }) => {
  const routes = Router();

  routes.use(jwtInjectMiddleware({ jwt }));

  return routes;
};
